from django.contrib import admin
from .models import Order, OrderItem, Transaction


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    fields = ('product', 'weight', 'quantity', 'cost',)


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'email', 'phone',
                    'address', 'status', 'total_cost',
                    'created', 'updated']
    list_editable = ['status']
    list_filter = ['status', 'created', 'updated']
    inlines = [OrderItemInline]
admin.site.register(Order, OrderAdmin)


class TransactionAdmin(admin.ModelAdmin):
    list_display = ['id', 'order', 'status', 'payment_id',
                    'json', 'created', 'updated']
    readonly_fields = ('order', 'status', 'payment_id', 'json')
    list_filter = ['status', 'order', 'created', 'updated']
admin.site.register(Transaction, TransactionAdmin)
