import json
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponse
from .models import Order, OrderItem, Transaction
from .forms import OrderCreateForm
from cart.cart import Cart
from opeshek.services import TelegramService
from opeshek.settings import DEBUG
import logging
import requests

from opeshek.settings import ORDER

logger = logging.getLogger(__name__)


def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save(commit=False)
            order.total_cost = cart.get_total_price()
            order.save()

            for item in cart:
                product = item['product']
                weight = int(item['weight'])
                quantity = int(item['quantity'])
                cost = product.get_price(weight) * quantity

                OrderItem.objects.create(order=order,
                                         product=product,
                                         weight=weight,
                                         quantity=quantity,
                                         cost=cost)
            return redirect('orders:order_pay', order_id=order.unique_id)
    else:
        form = OrderCreateForm
    return render(request, 'orders/order/create.html',
                  {'cart': cart, 'form': form})


def order_pay(request, order_id):
    order = get_object_or_404(Order, unique_id=order_id)

    receipt = {
        'Email': order.email,
        'Phone': order.phone,
        'Taxation': 'usn_income',
        'Items': []
    }

    order_items = OrderItem.objects.filter(order_id=order.id)
    for item in order_items:
        receipt['Items'].append({
            'Name': item.product.name,
            'Price': int(item.product.get_price(item.weight) * 100),
            'Quantity': float(item.quantity),
            'Amount': int(item.cost * 100),
            'Tax': 'none'
        })

    headers = {'Content-type': 'application/json;charset=UTF-8'}
    payload = {
        'TerminalKey': ORDER['TERMINAL_KEY'],
        'Amount': int(order.total_cost * 100),
        'OrderId': order_id,
        'Recurrent': 'N',
        'NotificationURL': ORDER['NOTIFICATION_URL'],
        'FailURL': '{}?OrderId={}'.format(ORDER['FAIL_URL'], order_id),
        'SuccessURL': '{}?OrderId={}'.format(ORDER['SUCCESS_URL'], order_id),
        'DATA': {
            'Phone': order.phone,
            'Email': order.email
        },
        'Receipt': receipt
    }
    response = requests.post("https://securepay.tinkoff.ru/v2/Init", headers=headers, json=payload)
    if response.status_code == 200:
        data = json.loads(response.content)
        if data.get('Success') and data.get('PaymentURL'):
            return redirect(data.get('PaymentURL'))

    return render(request, 'orders/order/fail.html',
                  {'order': order})


def order_fail(request):
    data = request.GET
    if data is None:
        logger.error('Request is none')
        return HttpResponse(status=404)

    logger.error('Order fail')
    logger.error(data)

    order_id = data.get('OrderId')
    order = get_object_or_404(Order, unique_id=order_id)

    return render(request, 'orders/order/fail.html',
                  {'order': order})


def order_success(request):
    data = request.GET
    if data is None:
        logger.error('Request is none')
        return HttpResponse(status=404)

    order_id = data.get('OrderId')
    order = get_object_or_404(Order, unique_id=order_id)

    cart = Cart(request)
    cart.clear()

    return render(request, 'orders/order/created.html',
                  {'order': order})


def order_callback(request):
    data = request.body
    if data is None:
        logger.error('Request is none')
        return HttpResponse(status=404)

    data = json.loads(request.body)
    success = data.get('Success')
    order_id = data.get('OrderId')
    status = data.get('Status')
    payment_id=data.get("PaymentId")

    order = Order.objects.get(unique_id=order_id)
    if order is None:
        logger.error('Order not found', order_id)
        return HttpResponse(status=404)

    Transaction.objects.create(
        order=order,
        status=status,
        payment_id=payment_id,
        json=data
    )

    if not success:
        logger.error('Success is False')
        order.status = 'ERROR_PAID'
        order.save()
        return HttpResponse(status=200)

    if order.status == 'PAID':
        logger.error('Order is already paid')
        return HttpResponse(status=200)

    if status == 'CONFIRMED':
        order.status = 'PAID'
        order.save()
        if not DEBUG:
            TelegramService().post('Оплачен заказ: https://opeshek.ru/adminorders/order/{}'.format(order.id))

    return HttpResponse(status=200)
