# Generated by Django 3.1.4 on 2021-03-22 16:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0008_auto_20210321_2028'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='paid',
        ),
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('New', 'Новый'), ('Paid', 'Оплачено')], default='New', max_length=10, verbose_name='Статус'),
        ),
    ]
