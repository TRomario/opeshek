# Generated by Django 3.1.4 on 2021-03-22 16:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0009_auto_20210322_2145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('NEW', 'Новый'), ('PAID', 'Оплачено'), ('NOT_PAID', 'Не оплачено'), ('DELIVERED', 'Доставляется'), ('RECEIVED', 'Получено')], default='NEW', max_length=10, verbose_name='Статус'),
        ),
    ]
