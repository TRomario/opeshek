import uuid

from django.db import models
from shop.models import Product


class Order(models.Model):
    STATUSES = (
        ('NEW', 'Новый'),
        ('PAID', 'Оплачено'),
        ('NOT_PAID', 'Не оплачено'),
        ('ERROR_PAID', 'Ошибка при оплате'),
        ('DELIVERED', 'Доставляется'),
        ('RECEIVED', 'Получено'),
    )

    unique_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=250, verbose_name='Контактное лицо')
    email = models.EmailField()
    address = models.CharField(max_length=250, verbose_name='Адрес')
    phone = models.CharField(max_length=50, verbose_name='Телефон')
    comment = models.TextField(blank=True, verbose_name='Комментарий')
    total_cost = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name='Общая стоимость')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    status = models.CharField(choices=STATUSES, max_length=10, default='NEW', verbose_name='Статус')

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return 'Order {}'.format(self.id)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE, verbose_name='Заказ')
    product = models.ForeignKey(Product, related_name='products', on_delete=models.CASCADE, verbose_name='Продукт')
    weight = models.IntegerField(default=1, verbose_name='Вес в граммах')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Количество')
    cost = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name='Стоимость')

    def __str__(self):
        return '{}'.format(self.id)


class Transaction(models.Model):
    STATUSES = (
        ('NEW', 'NEW'),
        ('AUTHORIZED', 'AUTHORIZED'),
        ('CONFIRMED', 'CONFIRMED'),
        ('REJECTED', 'REJECTED'),
    )

    order = models.ForeignKey(Order, related_name='orders', on_delete=models.CASCADE, verbose_name='Заказ')
    status = models.CharField(choices=STATUSES, max_length=10, default='NEW', verbose_name='Статус')
    payment_id = models.CharField(max_length=50, verbose_name='ID платежа')
    json = models.JSONField(null=True, verbose_name='json')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Транзакция'
        verbose_name_plural = 'Транзакции'

    def __str__(self):
        return 'Transaction {}'.format(self.id)
