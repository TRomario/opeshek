from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from . import views

app_name = 'orders'

urlpatterns = [
    url(r'^create/$', views.order_create, name='order_create'),
    url(r'^pay/(?P<order_id>[0-9a-z-]+)/$', views.order_pay, name='order_pay'),
    url(r'^fail/$', csrf_exempt(views.order_fail), name='order_fail'),
    url(r'^success/$', csrf_exempt(views.order_success), name='order_success'),
    url(r'^callback/$', csrf_exempt(views.order_callback), name='order_callback'),
]
