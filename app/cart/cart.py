from decimal import Decimal
from django.conf import settings

from shop.models import Product


class Cart(object):
    def __init__(self, request):
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart

    def __iter__(self):
        # product_ids = self.cart.keys()
        # products = Product.objects.filter(id__in=product_ids)
        # for product in products:
        #     self.cart[str(product.id)]['packing_price'] = product

        for weight in self.cart.values():
            for key, item in weight.items():
                item['product'] = Product.objects.get(id=item['product_id'])
                item['weight'] = key
                item['price'] = Decimal(item['price'])
                item['total_price'] = item['price'] * item['quantity']
                yield item

    def __len__(self):
        total = 0
        for weight in self.cart.values():
            total += sum(item['quantity'] for item in weight.values())
        return total

    def add(self, product, weight, quantity=1, update_quantity=False):
        product_id = str(product.id)
        weight_key = str(weight)
        if product_id not in self.cart:
            self.cart[product_id] = {
                weight_key: {
                    'product_id': str(product.id),
                    'quantity': 0,
                    'price': str(product.get_price(weight))
                }
            }
        elif weight_key not in self.cart[product_id]:
            self.cart[product_id][weight_key] = {
                'product_id': str(product.id),
                'quantity': 0,
                'price': str(product.get_price(weight))
            }
        if update_quantity:
            self.cart[product_id][weight_key]['quantity'] = quantity
        else:
            self.cart[product_id][weight_key]['quantity'] += quantity
        self.save()

    def remove(self, product, weight):
        product_id = str(product.id)
        if product_id in self.cart:
            del self.cart[product_id][weight]
            self.save()

    def save(self):
        self.session[settings.CART_SESSION_ID] = self.cart
        self.session.modified = True

    def clear(self):
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True

    def get_total_price(self):
        total = 0
        for weight in self.cart.values():
            total += sum(Decimal(item['price']) * item['quantity'] for item in weight.values())
        return total

    def get_total_product(self):
        total = 0
        for weight in self.cart.values():
            total += len(weight.values())
        return total
