from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST

from shop.models import Product
from .cart import Cart
from .forms import CartAddProductForm


@require_POST
def cart_add(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(product=product,
                 weight=cd['weight'],
                 quantity=cd['quantity'],
                 update_quantity=cd['update'])
    return JsonResponse({
        'total_price': cart.get_total_price(),
        'count_products': cart.get_total_product()
    })


def cart_remove(request, product_id, weight):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product, weight)
    return redirect('cart:cart_detail')


def cart_clear(request):
    cart = Cart(request)
    cart.clear()
    return redirect('cart:cart_detail')


def cart_detail(request):
    cart = Cart(request)
    return render(request, 'cart/detail.html', {'cart': cart})
