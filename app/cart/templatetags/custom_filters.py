from django import template

register = template.Library()


def weight(weight):
    if weight == 1000 or weight == '1000':
        return '1 кг'
    return '{} г'.format(weight)


register.filter('weight', weight)
