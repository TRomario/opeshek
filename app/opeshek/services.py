# coding=utf-8

import requests
from opeshek import settings


class TelegramService:
    def __init__(self):
        self.settings = settings.TELEGRAM
        self.chat_id = self.settings['chat_id']
        self.bot_token = self.settings['bot_token']

    def post(self, message):
        try:
            if message is not None:
                send_text = 'https://api.telegram.org/bot' + self.bot_token + '/sendMessage?chat_id=' + self.chat_id + '&parse_mode=Markdown&text=' + message
                response = requests.get(send_text)
                if response.status_code != 200:
                    print(response.json())
                return response.json()
        except Exception as ex:
            print(ex)
            return False
