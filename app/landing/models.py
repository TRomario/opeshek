from django.db import models


class Status(models.Model):
    name = models.CharField(max_length=250, verbose_name='Название')
    slug = models.SlugField(max_length=250, db_index=True, unique=True, verbose_name='Slug')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'

    def __str__(self):
        return '{}'.format(self.name)


class Claim(models.Model):
    STATUSES = (
        ('NEW', 'Новый'),
        ('PROCESSED', 'Обработан'),
        ('CANCELLED', 'Отменен'),
        ('REJECTED', 'Отказан'),
        ('PAY', 'Оплачивается'),
        ('PAID', 'Оплачен'),
        ('IN_DELIVERY', 'В доставку'),
        ('DELIVERED', 'Доставляется'),
        ('RETURN', 'Возврат'),
        ('READY', 'Готов'),
    )

    name = models.CharField(max_length=250, verbose_name='Контактное лицо')
    email = models.EmailField()
    phone = models.CharField(max_length=50, verbose_name='Телефон')
    status = models.CharField(choices=STATUSES, max_length=30, default='NEW', verbose_name='Статус')
    status_new = models.ForeignKey(Status, related_name='statuses', null=True, on_delete=models.CASCADE, verbose_name='Статус (новый)')
    comment = models.TextField(blank=True, verbose_name='Комментарий')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    class Meta:
        ordering = ('-created',)
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'

    def __str__(self):
        return 'Claim {}'.format(self.id)


class Product(models.Model):
    catalog = models.FileField(upload_to='catalog', blank=True, verbose_name='Каталог продуктов')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return 'Product {}'.format(self.id)
