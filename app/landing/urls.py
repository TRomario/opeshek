from django.urls import path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from .import views

app_name = 'landing'

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^create/$', views.claim_create, name='claim_create'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
