from django import forms
from .models import Claim


class ClaimCreateForm(forms.ModelForm):
    class Meta:
        model = Claim
        fields = ['name', 'phone', 'email']
