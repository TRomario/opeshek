from django.http import JsonResponse
from django.shortcuts import render
from .forms import ClaimCreateForm
from opeshek.services import TelegramService
from opeshek.settings import DEBUG
from landing.models import Product


def index(request):
    product = Product.objects.all().first()
    return render(request, "landing/partial/index.html", {'product': product})


def claim_create(request):
    if request.method == 'POST':
        form = ClaimCreateForm(request.POST)
        if form.is_valid():
            claim = form.save()
            if not DEBUG:
                TelegramService().post('Новая заявка: https://opeshek.ru/adminlanding/claim/{}'.format(claim.id))
            return JsonResponse({
                'success': True,
                'status': 'thanks'
            })
        else:
            return JsonResponse({
                'success': False,
                'status': 'ERROR',
                'msg': 'Ошибка валидации данных'
            })

    return JsonResponse({
        'success': False,
        'status': 'ERROR',
        'msg': 'Неизвестная ошибка'
    })
