from django.contrib import admin
from .models import Claim, Status, Product


class StatusAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}
    ordering = ('created',)
admin.site.register(Status, StatusAdmin)


class ClaimAdmin(admin.ModelAdmin):
    list_display = ['id', 'status', 'status_new', 'name', 'email', 'phone', 'comment', 'created', 'updated']
    list_filter = ['status', 'email', 'phone', 'created', 'updated']
    list_editable = ['status', 'status_new']
admin.site.register(Claim, ClaimAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ['id', 'created', 'updated']
admin.site.register(Product, ProductAdmin)
