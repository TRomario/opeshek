import os
from math import ceil

from django.db import models
from django.urls import reverse
from opeshek.storage import OverwriteStorage

from shop.utils import get_discount


def categories_path(instance, filename):
    filename, file_extension = os.path.splitext(filename)
    return 'categories/{0}{1}'.format(instance.slug, file_extension)


def products_path(instance, filename):
    filename, file_extension = os.path.splitext(filename)
    return 'products/{0}{1}'.format(instance.slug, file_extension)


class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True, verbose_name='Наименование')
    slug = models.SlugField(max_length=200, db_index=True, unique=True, verbose_name='Slug')
    image = models.ImageField(storage=OverwriteStorage(), upload_to=categories_path, blank=True, verbose_name='Изображение')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    class Meta:
        ordering = ('name',)
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('created',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category', args=[self.slug])


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE, verbose_name='Категория')
    name = models.CharField(max_length=200, db_index=True, verbose_name='Наименование')
    slug = models.SlugField(max_length=200, db_index=True, verbose_name='Slug')
    image = models.ImageField(storage=OverwriteStorage(), upload_to=products_path, blank=True, verbose_name='Изображение')
    description = models.TextField(blank=True, verbose_name='Описание')
    composition = models.TextField(blank=True, verbose_name='Состав')
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name='Цена за 150г')
    stock = models.PositiveIntegerField(verbose_name='В наличии')
    available = models.BooleanField(default=True, verbose_name='Доступно')
    bestseller = models.BooleanField(default=True, verbose_name='Хит-продаж')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    class Meta:
        ordering = ('name',)
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
        index_together = (('id', 'slug'),)
        ordering = ('name',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:product_detail',
                       args=[self.category.slug, self.slug])

    def get_short_description(self):
        if len(self.description) > 10:
            return self.description[0:10] + '...'
        return self.description

    def get_prices(self):
        return [
            { 'weight': 150, 'display': '150 г', 'price': self.get_price(150) },
            { 'weight': 300, 'display': '300 г', 'price': self.get_price(300) },
            { 'weight': 500, 'display': '500 г', 'price': self.get_price(500) },
            { 'weight': 1000, 'display': '1 кг', 'price': self.get_price(1000) }
        ]

    def get_price(self, weight):
        discount = get_discount(weight)
        price = self.price * weight / 150
        price = price - (price * discount / 100)
        return ceil(price)

    def get_first_price(self):
        return self.get_prices()[0]
