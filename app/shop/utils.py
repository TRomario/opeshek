def get_discount(weight):
    discount = 0
    if weight == 300:
        discount = 5
    elif weight == 500:
        discount = 7
    elif weight == 1000:
        discount = 10
    return discount
