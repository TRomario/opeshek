from django.utils.text import slugify
from unidecode import unidecode
from django.contrib import admin
from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget
from import_export.admin import ImportExportModelAdmin
from .models import Category, Product


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}
    ordering = ('created',)
admin.site.register(Category, CategoryAdmin)


class ProductResource(resources.ModelResource):
    category = fields.Field(
        column_name='category',
        attribute='category',
        widget=ForeignKeyWidget(Category, 'slug'))

    class Meta:
        model = Product
        skip_unchanged = True
        report_skipped = False
        exclude = ('image')
        fields = ('id', 'category', 'name', 'slug', 'description', 'composition', 'price', 'stock', 'available', 'bestseller')

    def before_import_row(self, row, row_number=None, **kwargs):
        if row.get('slug') is None:
            row['slug'] = slugify(unidecode(row.get('name')))
        return super().before_import_row(row, **kwargs)


class ProductAdmin(ImportExportModelAdmin):
    resource_class = ProductResource
    to_encoding= 'utf-8'
    from_encoding= 'utf-8'
    list_display = ['name', 'slug', 'category', 'stock', 'available', 'created', 'updated']
    list_filter = ['available', 'created', 'updated']
    list_editable = ['stock', 'available']
    prepopulated_fields = {'slug': ('name',)}
    ordering = ('name',)
admin.site.register(Product, ProductAdmin)
