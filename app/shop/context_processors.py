from shop.models import Category, Product


def category_list(request):
    categories = Category.objects.all()
    return {'categories': categories}


def search_query(request):
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']
        return {'query': q}
    return {'query': ''}
