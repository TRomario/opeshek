import bleach
from django.shortcuts import render, get_object_or_404
from shop.models import Category, Product
from cart.forms import CartAddProductForm


def home(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    bestsellers = Product.objects.filter(bestseller=True)

    return render(request,
                  'shop/partial/home.html',
                  {'category': category,
                   'categories': categories,
                   'products': products,
                   'bestsellers': bestsellers})

def product_list(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request,
                  'shop/product/list.html',
                  {'category': category,
                   'categories': categories,
                   'products': products})

def product_detail(request, category_slug, slug):
    product = get_object_or_404(Product,
                                slug=slug,
                                available=True)
    cart_product_form = CartAddProductForm()
    return render(request,
                  'shop/product/detail.html',
                  {'product': product,
                  'cart_product_form': cart_product_form})

def search(request):
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']
        products = Product.objects.filter(available=True, name__icontains=bleach.clean(q))
        return render(request,
                      'shop/product/search_results.html',
                      {'products': products, 'query': q})

    products = Product.objects.filter(available=True)
    return render(request,
                  'shop/product/search_results.html',
                  {'products': products})

def about(request):
    return render(request, "shop/partial/about.html")

def dostavka(request):
    return render(request, "shop/partial/dostavka.html")

def craftPackets(request):
    return render(request, "shop/partial/craft-packets.html")

def discounts(request):
    return render(request, "shop/partial/discounts.html")

def zakaz(request):
    return render(request, "shop/partial/kak-sdelat-zakaz.html")

def questions(request):
    return render(request, "shop/partial/voprosy-i-otvety.html")

def contact(request):
    return render(request, "shop/partial/contact.html")
