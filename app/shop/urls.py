from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from .import views

app_name = 'shop'

urlpatterns = [
    path('', views.home, name='home'),
    path('about', views.about, name='about'),
    path('dostavka', views.dostavka, name='dostavka'),
    path('craft-packets', views.craftPackets, name='craft-packets'),
    path('discounts', views.discounts, name='discounts'),
    path('kak-sdelat-zakaz', views.zakaz, name='zakaz'),
    path('voprosy-i-otvety', views.questions, name='questions'),
    path('contact', views.contact, name='contact'),

    path('all-foods',
         views.product_list,
         name='product_list'),

    path('search', views.search, name='search'),

    path('<str:category_slug>',
        views.product_list,
        name='product_list_by_category'),

    path('<str:category_slug>/<str:slug>',
         views.product_detail,
         name='product_detail'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
